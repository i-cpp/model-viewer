#include "common.h"
#include "shapes.h"

void Shapes::DrawSphere( const float center[], float radius, int slices, int stacks, int vertHalf )
{
	const int cacheSize = 240;
	if ( slices >= cacheSize ) slices = cacheSize - 1;
	float angle;
	float sinCache[cacheSize];
	float cosCache[cacheSize];
	for ( int i = 0; i < slices; i++ )
	{
		angle = 2 * S3D_PI * i / slices;
		sinCache[i] = sinf( angle );
		cosCache[i] = cosf( angle );
	}
	sinCache[slices] = sinCache[0];
	cosCache[slices] = cosCache[0];

	float height = S3D_PI;
	float stack0, stack1;
	int start_stacks = 0, end_stacks = stacks;
	if ( vertHalf == 0 ) end_stacks /= 2;
	else if ( vertHalf == 1 ) start_stacks = end_stacks / 2 + 1;

	float zLow, zHigh, radiusLow, radiusHigh;
	float step = 1.f / stacks;
	for ( int j = start_stacks; j <= end_stacks; j++ )
	{
		stack1 = height * j * step - 0.5f * height;
		stack0 = stack1 - height * step;
		
		zLow  = radius * sinf( stack0 );
		zHigh = radius * sinf( stack1 );
		radiusLow =  radius * cosf( stack0 );
		radiusHigh = radius * cosf( stack1 );
		
		std::vector< S3D_Vertex > verteces;
		for ( int i = 0; i <= slices; i++ )
		{
			verteces.push_back( S3D_Vertex( sinCache[i] * radiusHigh + center[0], cosCache[i] * radiusHigh + center[1], zHigh + center[2] ) );
			verteces.push_back( S3D_Vertex( sinCache[i] * radiusLow  + center[0], cosCache[i] * radiusLow  + center[1], zLow  + center[2] ) );
		}
		glEnableClientState( GL_VERTEX_ARRAY );
		glVertexPointer( 3, GL_FLOAT, 0, &verteces[0] );
		glDrawArrays( GL_TRIANGLE_STRIP, 0, verteces.size() );
		glDisableClientState( GL_VERTEX_ARRAY );
	}
}

void Shapes::DrawCylinder( float baseRadius, float topRadius, float height, int slices, int stacks )
{
	const int cacheSize = 240;
	if ( slices >= cacheSize ) slices = cacheSize - 1;
	float angle;
	float sinCache[cacheSize];
	float cosCache[cacheSize];
	for ( int i = 0; i < slices; i++ )
	{
		angle = 2 * S3D_PI * i / slices;
		sinCache[i] = sinf( angle );
		cosCache[i] = cosf( angle );
	}
	sinCache[slices] = sinCache[0];
	cosCache[slices] = cosCache[0];

	float deltaRadius = baseRadius - topRadius;

	float zLow, zHigh, radiusLow, radiusHigh;
	float step = 1.f / stacks;
	for ( int j = 0; j < stacks; j++ )
	{
		zLow = height * j * step - 0.5f * height;
		zHigh = zLow + height * step;
		radiusLow = baseRadius - deltaRadius * j * step;
		radiusHigh = radiusLow + deltaRadius * step;

		std::vector< S3D_Vertex > verteces;
		for ( int i = 0; i <= slices; i++ )
		{
			verteces.push_back( S3D_Vertex( radiusHigh * sinCache[i], radiusHigh * cosCache[i], zHigh ) );
			verteces.push_back( S3D_Vertex( radiusLow * sinCache[i], radiusLow * cosCache[i], zLow ) );
		}
		glEnableClientState( GL_VERTEX_ARRAY );
		glVertexPointer( 3, GL_FLOAT, 0, &verteces[0] );
		glDrawArrays( GL_TRIANGLE_STRIP, 0, verteces.size() );
		glDisableClientState( GL_VERTEX_ARRAY );
	}
}

void Shapes::DrawCapsule( float radius, float height, int slices, int stacks )
{
	float center[] = { 0, 0, -height * 0.5f };
	DrawSphere( center, radius, slices, stacks, 0 );
	DrawCylinder( radius, radius, height, slices, stacks );
	center[2] = -center[2];
	DrawSphere( center, radius, slices, stacks, 1 );
}

void Shapes::DrawGrid( float width, float height, float z, int slices, int stacks )
{
	std::vector< S3D_Vertex > verteces;
	float half, step;
	half = height * 0.5f;
	step = 1.f / stacks;
	for( int i = 0; i <= stacks; i++ )
	{
		verteces.push_back( S3D_Vertex(  half, -half + 2*half*i*step, z ) );
		verteces.push_back( S3D_Vertex( -half, -half + 2*half*i*step, z ) );
	}
	half = width * 0.5f;
	step = 1.f / slices;
	for( int j = 0; j <= slices; j++ )
	{
		verteces.push_back( S3D_Vertex( -half + 2*half*j*step,  half, z ) );
		verteces.push_back( S3D_Vertex( -half + 2*half*j*step, -half, z ) );
	}
	glEnableClientState( GL_VERTEX_ARRAY );
	glVertexPointer( 3, GL_FLOAT, 0, &verteces[0] );
	glDrawArrays( GL_LINES, 0, verteces.size() );
	glDisableClientState( GL_VERTEX_ARRAY );
}