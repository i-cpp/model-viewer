#include "common.h"

#include <gl/glext.h>
PFNGLGENERATEMIPMAPPROC GenerateMipmap = 0;

#include <png.h>
#include <zlib.h>
extern "C" { FILE __iob_func[3] = { *stdin,*stdout,*stderr }; }

typedef struct _texture {
	unsigned id;
	char *name;
	int w; int h;
	int bpp;
} TEXTURE;

const int g_maxTextures = 100;
int g_numTextures = 0;
TEXTURE* g_Textures[ g_maxTextures ] = { 0 };
bool g_bMipmapping = true;

int next_pow2( int a )
{
	int rval = 1;
	while( rval < a ) rval <<= 1;
	return rval;
}

unsigned int texture::total_size()
{
	unsigned int sum = 0;
	for ( int i = 0; i < g_numTextures; i++ )
	{
		sum += g_Textures[i]->w * g_Textures[i]->h * g_Textures[i]->bpp;
	}
	return sum;
}

unsigned int texture::load_from_data( unsigned char *data, int w, int h, int bpp, bool linearFilter, bool genMipMaps )
{
	GLenum format = 0;
	if ( bpp == 1 ) format = GL_LUMINANCE;
	else if ( bpp == 3 ) format = GL_RGB;
	else if ( bpp == 4 ) format = GL_RGBA;
	unsigned int Id;
	glEnable( GL_TEXTURE_2D );
	glGenTextures( 1, &Id );
	glBindTexture( GL_TEXTURE_2D, Id );
	glTexImage2D( GL_TEXTURE_2D, 0, bpp == 4 ? GL_RGBA : GL_RGB, w, h, 0, 
		bpp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, data );
	if ( !genMipMaps )
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, linearFilter ? GL_LINEAR : GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, linearFilter ? GL_LINEAR : GL_NEAREST );
	}
	else
	{
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, linearFilter ? GL_LINEAR : GL_NEAREST );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, linearFilter ? GL_LINEAR_MIPMAP_LINEAR : GL_NEAREST_MIPMAP_NEAREST );

		if ( !GenerateMipmap )
		{
			if ( S3D_OpenGLOp::ExtensionSupported( "GL_ARB_framebuffer_object" ) )
				S3D_GET_FUNC( PFNGLGENERATEMIPMAPPROC, GenerateMipmap, "glGenerateMipmap" )
			else if ( S3D_OpenGLOp::ExtensionSupported( "GL_EXT_framebuffer_object" ) )
				S3D_GET_FUNC( PFNGLGENERATEMIPMAPPROC, GenerateMipmap, "glGenerateMipmapEXT" )
		}
		if ( GenerateMipmap )
			GenerateMipmap( GL_TEXTURE_2D );
	}
	glBindTexture( GL_TEXTURE_2D, 0 );
	glDisable( GL_TEXTURE_2D );
	return Id;
}

unsigned int texture::load( const char *filename, S3D_Image *img, bool gen_texture )
{
	unsigned int texture = 0;
	if ( !img )
	{
		for ( int i = 0; i < g_numTextures; i++ )
		{
			if ( !strcmp( g_Textures[i]->name, filename ) )
			{
				return g_Textures[i]->id;
			}
		}
	}
	else
	{
		for ( int i = 0; i < g_numTextures; i++ )
		{
			if ( !strcmp( g_Textures[i]->name, filename ) )
			{
				texture = g_Textures[i]->id;
				break;
			}
		}
	}

	unsigned char *pixels = 0;
	int width, height, bpp;
	const char *ext = filename + strlen( filename ) - 4;
	if ( !strcmp( ext, ".png" ) )
	{
		png_image image;
		memset( &image, 0, ( sizeof image ) );
		image.version = PNG_IMAGE_VERSION;
		if ( png_image_begin_read_from_file( &image, filename ) )
		{
			bpp = PNG_IMAGE_SAMPLE_CHANNELS( image.format );
			image.format = bpp == 4 ? PNG_FORMAT_RGBA : PNG_FORMAT_RGB;
			width = image.width;
			height = image.height;
			pixels = (unsigned char*)malloc( PNG_IMAGE_SIZE( image ) );
			png_image_finish_read( &image, 0/*background*/, pixels, 0/*row_stride*/, 0/*colormap*/ );
		}
	}

	if ( pixels )
	{
		if ( gen_texture && !texture )
		{
			texture = load_from_data( pixels, width, height, bpp, true, g_bMipmapping );
			
			TEXTURE *tex = new TEXTURE;
			tex->id = texture;
			tex->name = _strdup( filename );
			tex->w = width;
			tex->h = height;
			tex->bpp = bpp;
			g_Textures[ g_numTextures ] = tex;
			g_numTextures++;
		}
		if ( img )
		{
			img->data = (unsigned char*)malloc( width * height * bpp );
			memcpy( img->data, pixels, width * height * bpp );
			img->width = width;
			img->height = height;
			img->bpp = bpp;
		}
		free( pixels );
	}
	return texture;
}

void texture::free_all()
{
	for ( int i = 0; i < g_numTextures; i++ )
	{
		if ( g_Textures[i] )
		{
			if ( glIsTexture( g_Textures[i]->id ) )
				glDeleteTextures( 1, &g_Textures[i]->id );
			if ( g_Textures[i]->name )
				free( g_Textures[i]->name );
			delete g_Textures[i];
			g_Textures[i] = 0;
		}
	}
}