#pragma once

struct DebugInfo {
	bool enabled = true;
	bool bboxes = false;
	bool pivot = false;
	bool skeletion = false;
#if ( S3D_USE_PHYSICS )
	bool collision_shape = false;
#endif
};

struct State {
	bool left_button = false;
	bool right_button = false;
	bool button_pressed = false;

	short mouse_whell = 0;
	POINT mouse_pos = { 0, 0 };
	POINT pressed_pos = { 0, 0 };

	int tris_number = 0;
	float video_mem_alloted = 0;

	bool arrow_keys[4] = {0};
	int wsad_keys[4] = {0};
	int space_key = 0;
	int control_state = 0;

	CS3DVec3f last_cam_rotation;
	CS3DVec3f last_cam_position;
	bool light_pos_as_cam_pos = false;

	GLenum polygon_mode = GL_FILL;
	bool tracking_camera = false;
	unsigned int last_animation_id = 0;
};

struct InterfaceImages {
	unsigned move_camera;
	unsigned move_xy;
	unsigned rotation3d;
};

struct SelectedModel {
	const char *name = "{null}";
	int id = 0;
};