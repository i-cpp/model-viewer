#include "common.h"
#include "renderer.h"

#include <GL/wglext.h>

wchar_t WindowName[0x100] = L"ModelViewer";
const wchar_t *WindowClassName = L"ModelViewer";
SIZE WindowSize = { 1024, 768 };

Renderer::Renderer( bool multiSampling )
: hRC( 0 )
, hDC( 0 )
, hIcon( 0 )
, FPS( 0 )
, FramesCount( 0 )
, LastTimeFPS( 0 )
, PixelFormat( 0 )
, MultiSampling( 0 )
{
	UseMultiSampling = multiSampling;
}

bool Renderer::IsSupported( const char *glExtension )
{
	const int ExtensionLength = strlen( glExtension );
	const char *Supported = 0;
	bool is_wgl = false;
	if ( PROC wglGetExtString = wglGetProcAddress( "wglGetExtensionsStringARB" ) )
	{
		is_wgl = true;
		Supported = ((char*(__stdcall*)(HDC))wglGetExtString)( wglGetCurrentDC() );
	}
	if ( !Supported )
	{
		if ( !( Supported = (char*)glGetString( GL_EXTENSIONS ) ) )
			return false;
	}
	for ( const char* c = Supported; ; c++ )
	{
		c = strstr( c, glExtension );
		if ( !c )
		{
			if ( is_wgl )
			{
				c = (char*)glGetString( GL_EXTENSIONS );
				is_wgl = false;
			}
			else return false;
		}
		if ( ( c == Supported || c[ -1 ] == ' ' ) 
		  && ( c[ ExtensionLength ] == '\0' || c[ ExtensionLength ] == ' ' ) )
			return true;
	}
}

bool Renderer::SetMultiSampling()
{
	if ( IsSupported( "WGL_ARB_multisample" ) )
	{
		PFNWGLCHOOSEPIXELFORMATARBPROC wglChoosePixelFormatARB = 
		(PFNWGLCHOOSEPIXELFORMATARBPROC)wglGetProcAddress( "wglChoosePixelFormatARB" );
		if ( wglChoosePixelFormatARB )
		{
			const int numSamples = 4;
			int	PixFmt;
			UINT NumFormats;
			float fAttributes[] = { 0,0 };
			int iAttributes[] =
			{
				WGL_DRAW_TO_WINDOW_ARB,		GL_TRUE,
				WGL_SUPPORT_OPENGL_ARB,		GL_TRUE,
				WGL_ACCELERATION_ARB,		WGL_FULL_ACCELERATION_ARB,
				WGL_COLOR_BITS_ARB,			24,
				WGL_ALPHA_BITS_ARB,			8,
				WGL_DEPTH_BITS_ARB,			24,
				WGL_STENCIL_BITS_ARB,		0,
				WGL_DOUBLE_BUFFER_ARB,		GL_TRUE,
				WGL_SAMPLE_BUFFERS_ARB,		GL_TRUE,
				WGL_SAMPLES_ARB,			numSamples,
				0,							0
			};
			int valid = wglChoosePixelFormatARB( hDC, iAttributes, fAttributes, 1, &PixFmt, &NumFormats );
			while ( NumFormats < 1 && iAttributes[11] > 0 )
			{
				iAttributes[11] -= 8;
				valid = wglChoosePixelFormatARB( hDC, iAttributes, fAttributes, 1, &PixFmt, &NumFormats );
			}
			if ( valid && NumFormats >= 1 ) glEnable( 0x809D ); //GL_MULTISAMPLE_ARB
			else
			{
				iAttributes[19] = 2;
				valid = wglChoosePixelFormatARB( hDC, iAttributes, fAttributes, 1, &PixFmt, &NumFormats );
				if ( valid && NumFormats >= 1 )
				{
					glEnable( 0x809D ); //GL_MULTISAMPLE_ARB
				}
			}
			if ( valid && NumFormats >= 1 )
			{
				PixelFormat = PixFmt;
				return true;
			}
		}
	}
	return false;
}

bool Renderer::Init( void (*callback)() )
{
	Free();
	if ( callback )
	{
		RenderScene = callback;
		WNDCLASSEX wcex = { 0 };
		wcex.cbSize		   = sizeof( WNDCLASSEXW );
		wcex.style         = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc   = MainWndProc;
		wcex.hInstance     = GetModuleHandle( 0 );
		wcex.hCursor	   = LoadCursor(0, IDC_ARROW);
		wcex.lpszClassName = WindowClassName;
		wcex.lpszMenuName = MAKEINTRESOURCE( IDR_MYMENU );
		wcex.hbrBackground = CreateSolidBrush( RGB( 0, 0, 0 ) );
		if( !RegisterClassEx( &wcex ) ) return false;
		hInstance = wcex.hInstance;

		RECT rcWnd;
		DWORD style = WS_CLIPCHILDREN | WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
		rcWnd.left = ( GetSystemMetrics( SM_CXSCREEN ) - WindowSize.cx ) / 2;
		rcWnd.top  = ( GetSystemMetrics( SM_CYSCREEN ) - WindowSize.cy ) / 2;
		rcWnd.right = rcWnd.left + WindowSize.cx;
		rcWnd.bottom = rcWnd.top + WindowSize.cy;

		AdjustWindowRect( &rcWnd, style, TRUE );
		hMainWnd = CreateWindowW( wcex.lpszClassName, WindowName, 
			style, rcWnd.left, rcWnd.top, rcWnd.right - rcWnd.left, rcWnd.bottom - rcWnd.top, 0, 0, hInstance, 0 );

		if( hMainWnd )
		{	
			hIcon = (HICON)LoadImage( wcex.hInstance, MAKEINTRESOURCE ( IDI_ICON ), IMAGE_ICON, 0, 0, LR_DEFAULTSIZE );
			SendMessage( hMainWnd, WM_SETICON, ICON_BIG, (LPARAM)hIcon );
			SendMessage( hMainWnd, WM_SETICON, ICON_SMALL, (LPARAM)hIcon );

			PIXELFORMATDESCRIPTOR pfd = { 0 };
			pfd.nSize = sizeof( PIXELFORMATDESCRIPTOR );
			pfd.nVersion = 1;
			pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW;
			pfd.iPixelType = PFD_TYPE_RGBA;
			pfd.cColorBits = 24;
			pfd.cDepthBits = 24;
			pfd.iLayerType = PFD_MAIN_PLANE;

			hDC = GetDC( hMainWnd );
			if ( !MultiSampling )
				PixelFormat = ChoosePixelFormat ( hDC, &pfd );
			if( PixelFormat && SetPixelFormat( hDC, PixelFormat, &pfd ) )
			{
				hRC = wglCreateContext( hDC );
				if ( hRC && wglMakeCurrent( hDC, hRC ) )
				{
					if ( !MultiSampling && UseMultiSampling )
					{
						MultiSampling = SetMultiSampling();
						if ( MultiSampling )
						{
							return Init( callback );
						}
					}
					glViewport( 0, 0, WindowSize.cx, WindowSize.cy );
					glMatrixMode( GL_PROJECTION );
					glLoadIdentity();
					glOrtho( 0, WindowSize.cx, WindowSize.cy, 0, -100.0, 100.0 );
					glMatrixMode( GL_MODELVIEW );
					glLoadIdentity();

					glEnable( GL_DEPTH_TEST );
					glDepthFunc( GL_LEQUAL );

					glShadeModel( GL_SMOOTH );
					glEnable ( GL_BLEND );
					glBlendFunc ( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
					glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
					
					glClearColor( 0.53f, 0.67f, 0.83f, 1 );
					glClearDepth( 1.0f );

					glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

					return true;
				}
			}
		}
	}
	return false;
}

void Renderer::RenderFrame()
{
	// waiting:
	GLfloat SleepTime = (
        10.0f - // maximum miliseconds per frame
        ( (GLfloat)real_time() - (GLfloat)CurTime ) // current miliseconds per frame
    );
	if ( SleepTime > 0 ) Sleep( (DWORD)SleepTime );
	CurTime = real_time();

	// calculate FPS:
	GLfloat sec = ( CurTime - LastTimeFPS ) * 0.001f;
	if( sec >= 1.0f )
	{
		LastTimeFPS = CurTime;
		FPS = FramesCount * sec;
		FramesCount = 0;
	}
	else FramesCount++;

	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	if ( RenderScene ) RenderScene();
	glFlush();
	SwapBuffers( hDC );
}

void Renderer::Free()
{
	if ( hIcon )
	{
		DestroyIcon( hIcon );
		hIcon = 0;
	}
	if( hRC )
	{
		wglMakeCurrent( 0, 0 );
		wglDeleteContext( hRC );
		hRC = 0;
	}
	if( hDC )
	{
		ReleaseDC( hMainWnd, hDC );
		hDC = 0;
	}
	if( hMainWnd )
	{
		DestroyWindow( hMainWnd );
		hMainWnd = 0;
	}
	if( hInstance )
	{
		UnregisterClassW( WindowClassName, hInstance );
		hInstance = 0;
	}
}

Renderer::~Renderer()
{
	Free();
}
