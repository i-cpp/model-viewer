#ifndef _renderer_h_
#define _renderer_h_

#include "resources.h"

LRESULT CALLBACK MainWndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );

class Renderer
{
public:
	Renderer( bool multiSampling = true );
	~Renderer();
	bool Init( void (*callback)() );
	void RenderFrame();
	inline float GetFPS() { return FPS; }
	inline bool Antialiasing() { return UseMultiSampling; }

private:
	void Free();
	bool IsSupported( const char *glExtension );
	bool SetMultiSampling();
	void (*RenderScene)();

	int PixelFormat;
	float FPS;
	HDC hDC;
	HGLRC hRC;
	HICON hIcon;
	unsigned int LastTimeFPS;
	int FramesCount;
	bool MultiSampling;
	bool UseMultiSampling;

};

#endif //_renderer_h_
 