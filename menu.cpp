#include "common.h"
#include "resources.h"
#include "menu.h"

#include <commctrl.h>

WNDPROC lpEditWndProc;

LRESULT CALLBACK EditProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	if( msg == WM_CHAR )
	{
		char numStr[0x10];
		GetWindowTextA( hWnd, numStr, 0x10 );
		bool ok = ( wParam >= 48 && wParam <= 57 ) || wParam == 8;
		if ( wParam == 46 /*.*/ )
		{
			if ( !strstr( numStr, "." ) )
			{
				ok = true;
			}
		}
		else if ( wParam == 45 /*-*/ )
		{
			char swap[0x100];
			if ( *numStr != '-' )
			{
				strcpy( swap, "-" );
				strcat( swap, numStr );
			}
			else
			{
				strcpy( swap, &numStr[1] );
			}
			SetWindowTextA( hWnd, swap );

			return false;
		}
		if ( !ok )
			return false;
	}
	return CallWindowProc( lpEditWndProc, hWnd, msg, wParam, lParam );
}

LRESULT CALLBACK DlgProc( HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch( msg )
	{
	case WM_INITDIALOG:
	{
#if ( S3D_USE_LIGTHING )
		int lightId = 0;
		S3D_Light *light = s3dLight->GetLight( lightId );
		if ( light )
		{
			SetDlgItemInt( hDlg, IDC_CHANGE_LIGHT_ID, lightId + 1, false );
			if ( s3dLight->NumLights() < 2 )
			{
				EnableWindow( GetDlgItem(hDlg, IDC_CHANGE_LIGHT_ID), false );
			}
			char numStr[0x10];
			sprintf( numStr, "%.01f", light->pos.x );
			SetDlgItemTextA( hDlg, IDC_CHANGE_LIGHT_POSX, numStr );
			sprintf( numStr, "%.01f", light->pos.y );
			SetDlgItemTextA( hDlg, IDC_CHANGE_LIGHT_POSY, numStr );
			sprintf( numStr, "%.01f", light->pos.z );
			SetDlgItemTextA( hDlg, IDC_CHANGE_LIGHT_POSZ, numStr );

			SetWindowLongPtr( GetDlgItem( hDlg, IDC_CHANGE_LIGHT_POSX ), 
				GWL_WNDPROC, (LONG_PTR)&EditProc );
			SetWindowLongPtr( GetDlgItem( hDlg, IDC_CHANGE_LIGHT_POSY ), 
				GWL_WNDPROC, (LONG_PTR)&EditProc );
			lpEditWndProc = (WNDPROC)SetWindowLongPtr( GetDlgItem( hDlg, IDC_CHANGE_LIGHT_POSZ ), 
				GWL_WNDPROC, (LONG_PTR)&EditProc );
		}
#endif
		return TRUE;
	}
	case WM_COMMAND:
	{
		switch(LOWORD(wParam))
		{
		case IDC_CHANGE_LIGHT_APPLY:
		{
#if ( S3D_USE_LIGTHING )
			BOOL translated;
			int lightId = (int)GetDlgItemInt( hDlg, IDC_CHANGE_LIGHT_ID, &translated, false ) - 1;
			S3D_Light *light = s3dLight->GetLight( lightId );
			if ( light )
			{
				char numStr[0x10];
				if ( GetDlgItemTextA( hDlg, IDC_CHANGE_LIGHT_POSX, numStr, 0x10 ) )
					light->pos.x = (float)atof( numStr );
				if ( GetDlgItemTextA( hDlg, IDC_CHANGE_LIGHT_POSY, numStr, 0x10 ) )
					light->pos.y = (float)atof( numStr );
				if ( GetDlgItemTextA( hDlg, IDC_CHANGE_LIGHT_POSZ, numStr, 0x10 ) )
					light->pos.z = (float)atof( numStr );
			}
#endif
			EndDialog(hDlg, 0);
		}
		break;
		case IDC_CHANGE_LIGHT_CANCEL:
		{
			EndDialog(hDlg, 0);
		}
		break;
		case IDC_CHANGE_LIGHT_POSX:
		{
			if ( HIWORD(wParam) == EN_CHANGE )
			{
				int qew =0 ;
			}
		}
		break;
		}
		return TRUE;
	}
	case WM_DESTROY:
	{
		EndDialog(hDlg, 0);
		return TRUE;
	}
	case WM_CLOSE:
	{
		EndDialog(hDlg, 0);
		return TRUE;
	}
	}
	return FALSE;
}

void CreateAppMenu(const State &state, const DebugInfo &debugInfo)
{
	HMENU hMenu, hSubMenu, hSubMenu2;
	hMenu = GetMenu( hMainWnd );


	hSubMenu = CreatePopupMenu();
	AppendMenu( hSubMenu, MF_STRING, ID_MODEL_OPEN, L"Open" );
	AppendMenu( hSubMenu, MF_SEPARATOR, 0, 0 );
#if ( S3D_USE_SHADOWS )
	if ( s3dShadow )
		AppendMenu( hSubMenu, s3dShadow->enabled ? MF_CHECKED : MF_UNCHECKED, ID_MODEL_SHADOW, L"Shadow" );
#endif
	AppendMenu( hSubMenu, state.tracking_camera ? MF_CHECKED : MF_UNCHECKED, ID_MODEL_TRACKING_CAMERA, L"Tracking Camera" );
	hSubMenu2 = CreatePopupMenu();
	AppendMenu( hSubMenu2, state.polygon_mode == GL_FILL ? MF_CHECKED : MF_UNCHECKED, ID_MODEL_POLY_MODE_FILL, L"Fill" );
	AppendMenu( hSubMenu2, state.polygon_mode == GL_LINE ? MF_CHECKED : MF_UNCHECKED, ID_MODEL_POLY_MODE_LINE, L"Line" );
	glPolygonMode( GL_FRONT_AND_BACK, state.polygon_mode );
	AppendMenu( hSubMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu2, L"Polygon Mode" );
	AppendMenu( hSubMenu, MF_SEPARATOR, 0, 0 );
	AppendMenu( hSubMenu, MF_STRING | MF_DISABLED, ID_MODEL_CLOSE, L"Close" );
	AppendMenu( hSubMenu, MF_STRING | MF_DISABLED, ID_MODEL_CLOSE_ALL, L"Close All" );
	AppendMenu( hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, L"Model" );

	hSubMenu = CreatePopupMenu();
	AppendMenu( hSubMenu, debugInfo.enabled ? MF_CHECKED : MF_UNCHECKED, ID_DEBUG_ENABLE, L"Enable" );
	AppendMenu( hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, L"Debug" );
	AppendMenu( hSubMenu, MF_SEPARATOR, 0, 0 );

	hSubMenu2 = CreatePopupMenu();
	AppendMenu( hSubMenu2, debugInfo.bboxes ? MF_CHECKED : MF_UNCHECKED, ID_DEBUG_MODEL_BBOXES, L"Bounding Boxes" );
	AppendMenu( hSubMenu2, debugInfo.pivot ? MF_CHECKED : MF_UNCHECKED, ID_DEBUG_MODEL_PIVOT, L"Pivot" );
	AppendMenu( hSubMenu2, debugInfo.skeletion ? MF_CHECKED : MF_UNCHECKED, ID_DEBUG_MODEL_SKELETON, L"Skeleton" );
#if ( S3D_USE_PHYSICS )
	AppendMenu( hSubMenu2, debugInfo.collision_shape ? MF_CHECKED : MF_UNCHECKED, ID_DEBUG_MODEL_COLLISION_SHAPE, L"Collision Shape" );
#endif
	AppendMenu( hSubMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu2, L"Model" );

#if ( S3D_USE_LIGTHING )
	hSubMenu = CreatePopupMenu();
	AppendMenu( hSubMenu, MF_STRING, ID_LIGHTING_LOAD, L"Load" );
	AppendMenu( hSubMenu, MF_STRING, ID_LIGHTING_CHANGE_POS, L"Change position" );
	AppendMenu( hSubMenu, state.light_pos_as_cam_pos ? MF_CHECKED : MF_UNCHECKED, ID_LIGHTING_AS_CAM_POS, L"As camera position" );
	AppendMenu( hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, L"Lighting" );
#endif

	hSubMenu2 = CreatePopupMenu();
	AppendMenu( hSubMenu2, s3dCamera->GetType() == S3D_FREE_CAMERA ? MF_CHECKED : MF_UNCHECKED, ID_CAMERA_TYPE_FREE, L"Free" );
	AppendMenu( hSubMenu2, s3dCamera->GetType() == S3D_TARGET_CAMERA ? MF_CHECKED : MF_UNCHECKED, ID_CAMERA_TYPE_TARGET, L"Target" );
	hSubMenu = CreatePopupMenu();
	AppendMenu( hSubMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu2, L"Reset" );
	AppendMenu( hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, L"Camera" );

	hSubMenu = CreatePopupMenu();
	AppendMenu( hSubMenu, MF_STRING, ID_HELP_ABOUT, L"About" );
	AppendMenu( hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, L"?" );
}