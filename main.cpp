#include "common.h"
#include "image_data.h"
#include "renderer.h"
#include "shapes.h"
#include "menu.h"

#include <commctrl.h>
#include <time.h>

OPENFILENAMEA ofn;
HINSTANCE hInstance = 0;
HWND hMainWnd = 0;
Renderer *g_Renderer = 0;
unsigned int CurTime = 0;

DebugInfo debugInfo;
InterfaceImages interfaceImages;
State state;
SelectedModel selectedModel;

void RenderScene();
void LoadModel( OPENFILENAMEA &ofn );

// MODELS:
const int g_maxModels = 100;
int g_numModels = 0;
CS3DModel *g_Models[ g_maxModels ] = { 0 };
char *g_ModelNames[ g_maxModels ] = { 0 };
int g_ModelGroup[ g_maxModels ] = { 0 };

unsigned int real_time() { return (unsigned int)GetTickCount(); }
inline unsigned int cur_time() { return CurTime; }
void *get_proc_addr( const char *name ) { return (void*)wglGetProcAddress( name ); }

CS3DBox groundBox;
const float groundSize = 500.f;
//#define TEST_BOXES
#if ( S3D_USE_PHYSICS )
const int numBoxes = 100;
#ifdef TEST_BOXES
CS3DBox testBoxes[numBoxes];
S3D_PhysicalObject boxes[numBoxes];
#endif
S3D_PhysicalObject player;
#endif

BOOL MainLoop()
{
	MSG msg = { 0 };
	while( ( msg.message != WM_QUIT ) )
	{
		if( PeekMessage( &msg, 0, 0, 0, PM_REMOVE ) )
		{
			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}
		else
		{
			if ( g_Renderer ) g_Renderer->RenderFrame();
		}
	}
	return msg.wParam;
}

void ResetCamera( S3D_CameraType type )
{
	s3dCamera->Reset( type );
	if ( type == S3D_FREE_CAMERA )
	{
		s3dCamera->SetPosition( S3D_AXIS_Y, 120 );
		s3dCamera->SetPosition( S3D_AXIS_Z, 1140 );
	}
	else if ( type == S3D_TARGET_CAMERA )
	{
		s3dCamera->SetPosition( S3D_AXIS_Y, 500 );
		s3dCamera->SetPosition( S3D_AXIS_Z, 900 );
	}
	s3dCamera->Update();

	state.last_cam_position = s3dCamera->GetPosition();
	state.last_cam_rotation = s3dCamera->GetRotation();
}

BOOL WINAPI WinMain( HINSTANCE hInst, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
	InitCommonControls();

	bool multisampling = false;
	LPWSTR cmd = GetCommandLine();
	int argc;
	LPWSTR *argv = CommandLineToArgvW( cmd, &argc );
	for ( int i = 1; i < argc; i++ )
	{
		if ( !wcscmp( argv[ i ], L"-multisampling" ) )
		{
			multisampling = true;
		}
	}

	int result = -1;
	g_Renderer = new Renderer( multisampling );
	if ( g_Renderer->Init( RenderScene ) )
	{
		s3dSetGetProcAddressFunc( get_proc_addr );
		if ( s3dInit() )
		{
			s3dSetTimeFunc( cur_time );
			s3dSetLoadTextureFunc( texture::load );
			s3dSetLoadTextureFunc( texture::load_from_data );
#if ( S3D_USE_LIGTHING )
			s3dLight->LoadLight( "models/light.cfg" );
#endif
			
			groundBox.Reset();
			groundBox.SetFillStyle( true );
			groundBox.Set( CS3DVec3f( groundSize, groundSize, 0.0f ) * .5f );
			groundBox.Set( CS3DVec3f( groundSize, groundSize, 200 ) * -.5f );
			groundBox.SetFillColor( S3D_Color( 0.6f, 0.6f, 0.6f, 1.0f ) );

#if ( S3D_USE_PHYSICS )
			s3dInitPhysics( CS3DVec3f( 0, 0, -9.81f * 100 ) );
			
			S3D_PhysicalObject ground;
			ground.mass = 0.f;
			ground.mat.translate( CS3DVec3f( 0, 0, -50 ) );
			s3dAddRigidBox( ground, groundBox.Size() );

			srand( real_time() );

#ifdef TEST_BOXES
			#define FRANDOM ( rand() % 1000 / 1000.f )
			// test physics
			for ( int i = 0; i < numBoxes; i++ )
			{
				S3D_PhysicalObject &box = boxes[i];
				box.mass = 1.f;
				box.mat.translate( CS3DVec3f( 
					groundSize * (FRANDOM - 0.5f), 
					groundSize * (FRANDOM - 0.5f), 
					50 + 200 * (FRANDOM - 0.5f) 
				));
				box.mat.rotate( 360 * FRANDOM, S3D_AXIS_X );
				box.mat.rotate( 360 * FRANDOM, S3D_AXIS_Z );
				box.mat.rotate( 360 * FRANDOM, S3D_AXIS_Y );
				s3dAddRigidBox( box, CS3DVec3f( 5 + 30 * FRANDOM, 5 + 30 * FRANDOM, 5 + 30 * FRANDOM ) );
				CS3DVec3f size = s3dGetAabbSize( box );
				testBoxes[i].Reset();
				testBoxes[i].SetFillStyle( true );
				testBoxes[i].Set( size * .5f );
				testBoxes[i].Set( size * -.5f );
				testBoxes[i].SetFillColor( S3D_Color( 0.6f, .4f, .4f, 1.0f ) );
			}
			#undef FRANDOM
#endif
#endif
			
			ResetCamera( S3D_TARGET_CAMERA );

			interfaceImages.move_camera = texture::load_from_data( (unsigned char *)MOVE_CAMERA_IMAGE_DATA, 
				MOVE_CAMERA_IMAGE_WIDTH, MOVE_CAMERA_IMAGE_HEIGHT, 4, false, false );
			interfaceImages.move_xy = texture::load_from_data( (unsigned char *)MOVE_XY_IMAGE_DATA, 
				MOVE_XY_IMAGE_WIDTH, MOVE_XY_IMAGE_HEIGHT, 4, false, false );
			interfaceImages.rotation3d = texture::load_from_data( (unsigned char *)ROTATION_3D_IMAGE_DATA, 
				ROTATION_3D_IMAGE_WIDTH, ROTATION_3D_IMAGE_HEIGHT, 4, false, false );

#if ( S3D_USE_SHADOWS )
			if ( s3dShadow )
				s3dShadow->enabled = false;
#endif
			CreateAppMenu(state, debugInfo);
			
			ShowWindow( hMainWnd, SW_NORMAL );
			UpdateWindow( hMainWnd );

			result = MainLoop();
		}
	}

	for ( int i = 0; i < g_numModels; i++ )
	{
		delete g_Models[i];
		free( g_ModelNames[i] );
	}
#if ( S3D_USE_PHYSICS )
	s3dFreePhysics();
#endif
	s3dFree();
	texture::free_all();

	// remove interface textures
	if ( glIsTexture( interfaceImages.move_camera ) )
		glDeleteTextures( 1, &interfaceImages.move_camera );
	if ( glIsTexture( interfaceImages.move_xy ) )
		glDeleteTextures( 1, &interfaceImages.move_xy );
	if ( glIsTexture( interfaceImages.rotation3d ) )
		glDeleteTextures( 1, &interfaceImages.rotation3d );

	delete g_Renderer;
	
	_CrtDumpMemoryLeaks();
	return result;
}

void LoadModel( OPENFILENAMEA &ofn )
{
	char szFileName[MAX_PATH];
	strcpy( szFileName, ofn.lpstrFile );
	ofn.lpstrFile += ofn.nFileOffset;
	char szFilePath[MAX_PATH];

	int group = 0;
	for ( int i = 0; i < g_numModels; i++ )
	{
		group = max( group, g_ModelGroup[i] );
	}
	group++;
	while ( ( *ofn.lpstrFile != 0 ) && ( ofn.nMaxFile != 0 ) )
	{
		strcpy(szFilePath,szFileName);
		DWORD dwAttrs;
		dwAttrs = GetFileAttributesA(szFilePath);
		if (dwAttrs & FILE_ATTRIBUTE_DIRECTORY)
		{
			strcat(szFilePath,(const char *)"\\");
			strcat(szFilePath,ofn.lpstrFile);
		}

		CS3DModel *model = new CS3DModel;
		model->Load( szFilePath );
		int numTris = model->TrianglesCount();
		if ( numTris )
		{
			state.tris_number += numTris;
			model->SetPosition( model->GetPivot() );

			g_ModelGroup[ g_numModels ] = group;
			g_Models[ g_numModels ] = model;
			// get model name:
			int nameLen = strlen( szFilePath );
			int i;
			for ( i = nameLen - 1; i >= 0; i-- )
				if ( szFilePath[i] == '\\' ) { ++i; break; }
			nameLen = nameLen - i - 4;
			g_ModelNames[ g_numModels ] = (char*)calloc( nameLen + 1, sizeof( char ) );
			memcpy( g_ModelNames[ g_numModels ], &szFilePath[i], nameLen * sizeof( char ) );
			g_numModels++;
			EnableMenuItem( GetMenu( hMainWnd ), ID_MODEL_CLOSE, MF_ENABLED );
			EnableMenuItem( GetMenu( hMainWnd ), ID_MODEL_CLOSE_ALL, MF_ENABLED );

#if ( S3D_USE_PHYSICS )
			player.handle = 0;
#endif
		}
		else
		{
			MessageBoxA( hMainWnd, "Can't load the model.", "Error", 0 );
			delete model;
		}

		while ( *(ofn.lpstrFile++) && (--ofn.nMaxFile) );
	}
}

LRESULT CALLBACK MainWndProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	switch( msg )
	{
		case WM_COMMAND:
            switch( LOWORD(wParam) )
            {
                case ID_MODEL_OPEN:
					{
						char filename [ MAX_PATH ] = { 0 };
						memset( &ofn, 0, sizeof( OPENFILENAMEA ) );
						ofn.lStructSize = sizeof ( OPENFILENAME );
						ofn.Flags = OFN_ENABLESIZING | OFN_EXPLORER | OFN_NOCHANGEDIR | OFN_OVERWRITEPROMPT | OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT;
						ofn.hInstance = GetModuleHandle ( 0 );
						ofn.lpstrFilter = "Config file (.cfg)\0*.cfg;\0\0";
						ofn.nFilterIndex = 0;
						ofn.hwndOwner = 0;
						ofn.nMaxFile = MAX_PATH;
						ofn.lpstrFile = filename;

						ofn.lpstrInitialDir = ".\\models";
						ofn.lpstrTitle = "Open";
						ofn.lpstrDefExt = "*";
						
						if ( GetOpenFileNameA ( &ofn ) )
						{
							LoadModel( ofn );
						}
						else return 0;
					}
					break;
                case ID_MODEL_CLOSE:
					{
						if ( g_numModels )
						{
							g_numModels--;
							state.tris_number -= g_Models[ g_numModels ]->TrianglesCount();
							delete g_Models[ g_numModels ];
							g_Models[ g_numModels ] = 0;
							free( g_ModelNames[ g_numModels ] );
							g_ModelNames[ g_numModels ] = 0;
							if ( !g_numModels )
							{
								EnableMenuItem( GetMenu( hWnd ), ID_MODEL_CLOSE, MF_DISABLED );
								EnableMenuItem( GetMenu( hWnd ), ID_MODEL_CLOSE_ALL, MF_DISABLED );
							}
						}
					}
					break;
                case ID_MODEL_CLOSE_ALL:
					{
						for ( int i = 0; i < g_numModels; i++ )
						{
							delete g_Models[ i ];
							g_Models[ i ] = 0;
							free( g_ModelNames[ i ] );
							g_ModelNames[ i ] = 0;
						}
						g_numModels = 0;
						state.tris_number = 0;
						EnableMenuItem( GetMenu( hWnd ), ID_MODEL_CLOSE, MF_DISABLED );
						EnableMenuItem( GetMenu( hWnd ), ID_MODEL_CLOSE_ALL, MF_DISABLED );
					}
					break;
#if ( S3D_USE_SHADOWS )
                case ID_MODEL_SHADOW:
					{
						s3dShadow->enabled = !s3dShadow->enabled;
						CheckMenuItem( GetMenu( hWnd ), ID_MODEL_SHADOW, s3dShadow->enabled ? MF_CHECKED : MF_UNCHECKED );
					}
					break;
#endif
				case ID_MODEL_TRACKING_CAMERA:
					{
						state.tracking_camera = !state.tracking_camera;
						CheckMenuItem( GetMenu( hWnd ), ID_MODEL_TRACKING_CAMERA, state.tracking_camera ? MF_CHECKED : MF_UNCHECKED );
					}
					break;
				case ID_MODEL_POLY_MODE_FILL:
					{
						state.polygon_mode = GL_FILL;
						glPolygonMode( GL_FRONT_AND_BACK, state.polygon_mode );
						CheckMenuItem( GetMenu( hWnd ), ID_MODEL_POLY_MODE_FILL, MF_CHECKED );
						CheckMenuItem( GetMenu( hWnd ), ID_MODEL_POLY_MODE_LINE, MF_UNCHECKED );
					}
					break;
				case ID_MODEL_POLY_MODE_LINE:
					{
						state.polygon_mode = GL_LINE;
						glPolygonMode( GL_FRONT_AND_BACK, state.polygon_mode );
						CheckMenuItem( GetMenu( hWnd ), ID_MODEL_POLY_MODE_LINE, MF_CHECKED );
						CheckMenuItem( GetMenu( hWnd ), ID_MODEL_POLY_MODE_FILL, MF_UNCHECKED );
					}
					break;
                case ID_FILE_EXIT:
					SendMessage( hWnd, WM_CLOSE, 0, 0 );
					break;
                case ID_DEBUG_ENABLE:
					{
						debugInfo.enabled = !debugInfo.enabled;
						CheckMenuItem( GetMenu( hWnd ), ID_DEBUG_ENABLE, debugInfo.enabled ? MF_CHECKED : MF_UNCHECKED );
					}
					break;
                case ID_DEBUG_MODEL_BBOXES:
					{
						debugInfo.bboxes = !debugInfo.bboxes;
						CheckMenuItem( GetMenu( hWnd ), ID_DEBUG_MODEL_BBOXES, debugInfo.bboxes ? MF_CHECKED : MF_UNCHECKED );
					}
					break;
				case ID_DEBUG_MODEL_PIVOT:
					{
						debugInfo.pivot = !debugInfo.pivot;
						CheckMenuItem( GetMenu( hWnd ), ID_DEBUG_MODEL_PIVOT, debugInfo.pivot ? MF_CHECKED : MF_UNCHECKED );
					}
					break;
				case ID_DEBUG_MODEL_SKELETON:
					{
						debugInfo.skeletion = !debugInfo.skeletion;
						CheckMenuItem( GetMenu( hWnd ), ID_DEBUG_MODEL_SKELETON, debugInfo.skeletion ? MF_CHECKED : MF_UNCHECKED );
					}
					break;
#if ( S3D_USE_PHYSICS )
				case ID_DEBUG_MODEL_COLLISION_SHAPE:
					{
						debugInfo.collision_shape = !debugInfo.collision_shape;
						CheckMenuItem( GetMenu( hWnd ), ID_DEBUG_MODEL_COLLISION_SHAPE, debugInfo.collision_shape ? MF_CHECKED : MF_UNCHECKED );
					}
					break;
#endif
#if ( S3D_USE_LIGTHING )
				case ID_LIGHTING_LOAD:
					{
						char filename [ MAX_PATH ] = { 0 };
						memset( &ofn, 0, sizeof( OPENFILENAMEA ) );
						ofn.lStructSize = sizeof ( OPENFILENAME );
						ofn.Flags = OFN_ENABLESIZING | OFN_EXPLORER | OFN_NOCHANGEDIR | OFN_OVERWRITEPROMPT | OFN_HIDEREADONLY;
						ofn.hInstance = GetModuleHandle ( 0 );
						ofn.lpstrFilter = "Config file (.cfg)\0*.cfg;\0\0";
						ofn.nFilterIndex = 0;
						ofn.hwndOwner = 0;
						ofn.nMaxFile = MAX_PATH;
						ofn.lpstrFile = filename;

						ofn.lpstrInitialDir = ".\\models";
						ofn.lpstrTitle = "Open";
						ofn.lpstrDefExt = "*";
						
						if ( GetOpenFileNameA ( &ofn ) )
						{
							s3dLight->LoadLight( ofn.lpstrFile );
						}
						else return 0;
					}
					break;
				case ID_LIGHTING_CHANGE_POS:
					{
						DialogBox( hInstance, MAKEINTRESOURCE(IDD_CHANGE_LIGHT_POS), 
							hMainWnd, reinterpret_cast<DLGPROC>(DlgProc) );
					}
					break;
				case ID_LIGHTING_AS_CAM_POS:
					{
						state.light_pos_as_cam_pos = !state.light_pos_as_cam_pos;
						CheckMenuItem( GetMenu( hWnd ), ID_LIGHTING_AS_CAM_POS, state.light_pos_as_cam_pos ? MF_CHECKED : MF_UNCHECKED );
					}
					break;
#endif
				case ID_CAMERA_TYPE_FREE:
					{
						ResetCamera( S3D_FREE_CAMERA );
						CheckMenuItem( GetMenu( hWnd ), ID_CAMERA_TYPE_FREE, MF_CHECKED );
						CheckMenuItem( GetMenu( hWnd ), ID_CAMERA_TYPE_TARGET, MF_UNCHECKED );
					}
					break;
				case ID_CAMERA_TYPE_TARGET:
					{
						ResetCamera( S3D_TARGET_CAMERA );
						CheckMenuItem( GetMenu( hWnd ), ID_CAMERA_TYPE_TARGET, MF_CHECKED );
						CheckMenuItem( GetMenu( hWnd ), ID_CAMERA_TYPE_FREE, MF_UNCHECKED );
					}
					break;
				case ID_HELP_ABOUT:
					MessageBoxA( hWnd, "ModelViewer Version 1.0.0\n"
									   "� 2012-2014 ID Company. All rights reserved.\n"
									   "Programming by Igor Kubyshkin.\n", "About", 0 );
					break;
            }
        break;
		case WM_CLOSE:
		PostQuitMessage( 0 );
		return 0;
		case WM_KEYDOWN:
		{
			if ( wParam >= 37 && wParam <= 40 /*left, right, up, down*/ ) state.arrow_keys[ wParam - 37 ] = 1;
			else if ( wParam == 65 /*a*/ && state.wsad_keys[0] == 0 )
			{
				state.wsad_keys[0] = 1;
			}
			else if ( wParam == 68 /*d*/ && state.wsad_keys[2] == 0 )
			{
				state.wsad_keys[2] = 1;
			}
			else if ( wParam == 87 /*w*/ && state.wsad_keys[1] == 0 )
			{
				state.wsad_keys[1] = 1;
			}
			else if ( wParam == 83 /*s*/ && state.wsad_keys[3] == 0 )
			{
				state.wsad_keys[3] = 1;
			}
			else if ( wParam == 32 /*space*/ && state.space_key == 0 ) state.space_key = 1;
			else if ( wParam == 27 /*ESC*/ )
			{
				PostQuitMessage( 0 );
				return 0;
			}
		}
		break;
		case WM_KEYUP:
		{
			if ( wParam >= 37 && wParam <= 40 /*left, right, up, down*/ ) state.arrow_keys[ wParam - 37 ] = 0;
			else if ( wParam == 65 /*a*/ && state.wsad_keys[0] != 0 )
			{
				state.wsad_keys[0] = 0;
			}
			else if ( wParam == 68 /*d*/ && state.wsad_keys[2] != 0 )
			{
				state.wsad_keys[2] = 0;
			}
			else if ( wParam == 87 /*w*/ && state.wsad_keys[1] != 0 )
			{
				state.wsad_keys[1] = 0;
			}
			else if ( wParam == 83 /*s*/ && state.wsad_keys[3] != 0 )
			{
				state.wsad_keys[3] = 0;
			}
			else if ( wParam == 32 /*space*/ && state.space_key != 0 ) state.space_key = 0;
			else if ( wParam == 78 /*n*/ )
			{
				if ( selectedModel.id >= 0 )
				{
					for ( int j = 0; j < g_numModels; j++ )
					{
						if ( g_ModelGroup[selectedModel.id] == g_ModelGroup[j] )
						{
							CS3DModel *m = g_Models[ j ];
							state.last_animation_id = m->AnimationId() + 1;
							if ( !m->SetAnimation( state.last_animation_id ) )
							{
								m->SetAnimation( 0 );
								state.last_animation_id = 0;
							}
						}
					}
				}
			}
			
			if ( g_Models[0] && !state.wsad_keys[0] && !state.wsad_keys[1] && !state.wsad_keys[2] && !state.wsad_keys[3] && wParam != 78 )
			{
				for ( int j = 0; j < g_numModels; j++ )
				{
					if ( g_ModelGroup[0] == g_ModelGroup[j] )
					{
						g_Models[j]->SetAnimation( state.last_animation_id );
					}
				}
			}
		}
		break;
		case WM_LBUTTONDOWN:
		{
			SetCapture( hWnd );
			if ( !state.button_pressed )
			{
				state.button_pressed = true;
				state.pressed_pos = state.mouse_pos;
			}
			state.left_button = true;
		}
		break;
		case WM_LBUTTONUP:
		{
			state.button_pressed = false;
			state.left_button = false;
			ReleaseCapture();
		}
		break;
		case WM_RBUTTONDOWN:
		{
			SetCapture( hWnd );
			if ( !state.button_pressed )
			{
				state.button_pressed = true;
				state.pressed_pos = state.mouse_pos;
			}
			state.right_button = true;
		}
		break;
		case WM_CREATE:
		{
			HMENU hMenu = CreateMenu();
			HMENU hSubMenu = CreatePopupMenu();
			AppendMenu( hSubMenu, MF_STRING, ID_FILE_EXIT, L"Exit" );
			AppendMenu( hMenu, MF_STRING | MF_POPUP, (UINT)hSubMenu, L"File" );
			SetMenu( hWnd, hMenu );
		}
		break;
		case WM_ACTIVATE:
		{
			state.left_button = state.right_button = false;
		}
		break;
		case WM_RBUTTONUP:
		{
			state.last_cam_rotation = s3dCamera->GetRotation();
			state.button_pressed = false;
			state.right_button = false;
			ReleaseCapture();
		}
		break;
		case WM_MOUSEMOVE:
		{
			GetCursorPos( &state.mouse_pos );
			ScreenToClient( hWnd, &state.mouse_pos );
		}
		break;
		case WM_MOUSEWHEEL:
		{
			state.mouse_whell = GET_WHEEL_DELTA_WPARAM(wParam);
		}
		break;
		default: { /* do nothing */ }
	}
	return CallWindowProc( (WNDPROC)DefWindowProc, hWnd, msg, wParam, lParam );
}

void PushMatrices()
{
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
}

void PopMatrices()
{
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();
}

void DrawLightSource()
{
#if ( S3D_USE_LIGTHING )
	CS3DMat16f mat = s3dCamera->Get();
	CS3DVec3f lpos = s3dLight->GetLight( 0 )->pos;
	mat.translate( lpos );
	glLoadMatrixf( (float*)&mat );
	glColor4f( 1, 1, 1, 1 );
	float center[] = { 0, 0, 0 };
	Shapes::DrawSphere( center, 10, 8, 8 );
#endif
}

void DrawGround()
{
	CS3DMat16f mvMat;
	mvMat.identity();
	mvMat.rotate( -90.0f, S3D_AXIS_X );
	mvMat = mvMat * s3dCamera->Get();
	glLoadMatrixf( (float*)&mvMat );

	// draw grid
	glColor4f( 0, 0, 0, 1 );
	Shapes::DrawGrid( groundSize, groundSize, 0.3f, 20, 20 );
	//*/

	groundBox.Draw();
	
#if ( S3D_USE_PHYSICS )
#ifdef TEST_BOXES
	for ( int i = 0; i < numBoxes; i++ )
	{
		CS3DBox &box = testBoxes[i];
		S3D_PhysicalObject &physBox = boxes[i];
		s3dUpdatePhysicalObject( physBox );
		CS3DMat16f *rotMat = box.GetRotation();
		memcpy( rotMat, &physBox.mat, sizeof( float ) * 12 );
		box.SetPosition( S3D_AXIS_X, physBox.mat[12] );
		box.SetPosition( S3D_AXIS_Y, physBox.mat[13] );
		box.SetPosition( S3D_AXIS_Z, physBox.mat[14] );
		box.Draw();
	}
#endif
#endif
}

void DrawTexturedRect( float w, float h )
{
	glBegin( GL_QUADS );
		glTexCoord2f( 0, 0 );
		glVertex2f( 0, 0 );
		glTexCoord2f( 0, 1 );
		glVertex2f( 0, h );
		glTexCoord2f( 1, 1 );
		glVertex2f( w, h );
		glTexCoord2f( 1, 0 );
		glVertex2f( w, 0 );
	glEnd();
}

void DrawAxis( S3D_ENUM axis )
{
	float len = 5.0f;
	float verts[ 18 ] = { 0.0f };
	int offset = ( axis == S3D_AXIS_Z ? S3D_AXIS_Z : 0 );
	verts[3+axis] = len;
	verts[6+axis] = len;
	verts[9+axis] = len * 0.8f;
	verts[11-offset] = len * 0.05f;
	verts[12+axis] = len;
	verts[15+axis] = len * 0.8f;
	verts[17-offset] = -len * 0.05f;
	float colorRGB[4] = { 0, 0, 0, 1 };
	colorRGB[axis] = 1;
	s3dSimpleShader->SetUniformf( "u_color", 4, colorRGB );
	s3dSimpleShader->SetAttribPointerf( 0, 3, verts );
	S3D_OpenGLOp::DrawPrimitives( S3D_LINES, 0, 6 );
}

void PrintInfo()
{
	char buffer[0x100] = { 0 };
	int row = 10;
	int col = 350;

	glDisable( GL_TEXTURE_2D );
	glColor4f( 1.0f, 1.0f, 1.0f, 1.0f );

	sprintf( buffer, "FPS = %.01f", g_Renderer->GetFPS() );
	print_text( 20, row+=20, buffer, 1 );

	if ( debugInfo.enabled )
	{
		state.video_mem_alloted += (float)texture::total_size();
		state.video_mem_alloted = state.video_mem_alloted / 1024.0f / 1024.0f;
		sprintf( buffer, "VRAM = %.01f Mb", state.video_mem_alloted );
		print_text( 20, row+=20, buffer, 1 );
		row+=20;

		print_text( 20, row+=20, "Scene:", 1 );
		sprintf( buffer, " Near Clipping Plane = %.01f", S3D_NEAR );
		print_text( 20, row+=20, buffer, 1 );
		sprintf( buffer, " Far Clipping Plane = %.01f", S3D_FAR );
		print_text( 20, row+=20, buffer, 1 );
		sprintf( buffer, " Triangles Number = %i", state.tris_number );
		print_text( 20, row+=20, buffer, 1 );
		row+=20;

		print_text( 20, row+=20, "Camera:", 1 );
		sprintf( buffer, " Viewing Angle = %i%%", (int)S3D_FOVY );
		print_text( 20, row+=20, buffer, 1 );
		sprintf( buffer, " Position = [%.01f, %.01f, %.01f]", s3dCamera->GetPosition().x, 
			s3dCamera->GetPosition().y, s3dCamera->GetPosition().z );
		print_text( 20, row+=20, buffer, 1 );
		row+=20;

		print_text( 20, row+=20, "Lighting:", 1 );
		sprintf( buffer, " MAX_LIGHTS = %i", S3D_MAX_LIGHTS );
#if ( S3D_USE_LIGTHING )
		print_text( 20, row+=20, " enabled", 1 );
		print_text( 20, row+=20, buffer, 1 );
		for ( int i = 0; i < s3dLight->NumLights(); i++ )
		{
			sprintf( buffer, " Light Source #%i", i + 1 );
			print_text( 20, row+=20, buffer, 1 );
			S3D_Light *l = s3dLight->GetLight(i);
			sprintf( buffer, "   Position = [%.01f, %.01f, %.01f]", l->pos.x, l->pos.y, l->pos.z );
			print_text( 20, row+=20, buffer, 1 );
		}
#else
		print_text( 20, row+=20, " disabled", 1 );
#endif
		row+=20;

		if ( selectedModel.id >= 0 && g_Models[selectedModel.id] )
		{
			row = 10;
			
			print_text( 20 + col, row+=20, "Current selection:", 1 );
			sprintf( buffer, " Model name = %s", selectedModel.name );
			print_text( 20 + col, row+=20, buffer, 1 );
			const char *selectedBone = selectedModel.id >= 0 && g_Models[selectedModel.id] ? g_Models[selectedModel.id]->GetSelectedBoneName() : "";
			if ( !*selectedBone ) selectedBone = "{null}";
			sprintf( buffer, " Bone name = %s", selectedBone );
			print_text( 20 + col, row+=20, buffer, 1 );
			row+=20;

			if ( g_Models[selectedModel.id]->AnimationCount() )
			{
				print_text( 20 + col, row+=20, "Animation:", 1 );
				print_text( 20 + col, row+=20, " Show next (N)", 1 );
				sprintf( buffer, " Frame = %i", g_Models[selectedModel.id]->CurrentFrame() );
				print_text( 20 + col, row+=20, buffer, 1 );
				sprintf( buffer, " Frames = %i", g_Models[selectedModel.id]->NumFrames() );
				print_text( 20 + col, row+=20, buffer, 1 );
			}
		}

		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
		glPushMatrix();
		if ( glIsTexture( interfaceImages.move_camera ) )
		{
			glLoadIdentity();
			int indent = 4;
			float size = 32;
			glTranslated( WindowSize.cx - ( size + indent ) * 3 - indent - 30, WindowSize.cy - size - 46, 0 );
			print_text( 3, int(size) + indent + 18, "move camera", 1 );
			glEnable( GL_TEXTURE_2D );
			glBindTexture( GL_TEXTURE_2D, interfaceImages.move_camera );

			glTranslated( size * .5, size * .5, 0 );
			glRotated( -90, 0, 0, 1 );
			glTranslated( -size * .5, -size * .5, 0 );
			DrawTexturedRect( size, size );
			
			glTranslated( 0, size + indent, 0 );
			glTranslated( size * .5, size * .5, 0 );
			glRotated( -90, 0, 0, 1 );
			glTranslated( -size * .5, -size * .5, 0 );
			DrawTexturedRect( size, size );
			
			glTranslated( 0, size + indent, 0 );
			glTranslated( size * .5, size * .5, 0 );
			glRotated( 180, 0, 0, 1 );
			glTranslated( -size * .5, -size * .5, 0 );
			DrawTexturedRect( size, size );

			glTranslated( size + indent, ( size + indent ), 0 );
			glTranslated( size * .5, size * .5, 0 );
			glRotated( 90, 0, 0, 1 );
			glTranslated( -size * .5, -size * .5, 0 );
			DrawTexturedRect( size, size );

			glDisable( GL_TEXTURE_2D );
		}
		if ( glIsTexture( interfaceImages.move_xy ) )
		{
			glLoadIdentity();
			float size = 64;
			glTranslated( 30, WindowSize.cy - size - 43, 0 );
			print_text( 0, (int)size + 18, "left button", 1 );
			glEnable( GL_TEXTURE_2D );
			glBindTexture( GL_TEXTURE_2D, interfaceImages.move_xy );
			DrawTexturedRect( size, size );
			glDisable( GL_TEXTURE_2D );
		}
		if ( glIsTexture( interfaceImages.rotation3d ) )
		{
			glLoadIdentity();
			float size = 64;
			glTranslated( 170, WindowSize.cy - size - 43, 0 );
			print_text( -20, (int)size + 18, "right button", 1 );
			glEnable( GL_TEXTURE_2D );
			glBindTexture( GL_TEXTURE_2D, interfaceImages.rotation3d );
			DrawTexturedRect( size, size );
			glDisable( GL_TEXTURE_2D );
		}
		glPopMatrix();
		glPolygonMode( GL_FRONT_AND_BACK, state.polygon_mode );

		///*
		if ( s3dSimpleShader )
		{
			s3dSimpleShader->BeginShaderProgram();
			s3dSimpleShader->EnableAttribute( 0 );

			CS3DMat16f mMat;
			mMat.translate( CS3DVec3f(23,15,-100) );
			CS3DMat16f mvMat = s3dCamera->GetRotationMat() * mMat;
			CS3DMat16f mvpMat = mvMat * s3dProjection;
			s3dSimpleShader->SetUniformMatrixf( "mvpMat", 4, (float*)&mvpMat );
			DrawAxis( S3D_AXIS_X );
			DrawAxis( S3D_AXIS_Y );
			DrawAxis( S3D_AXIS_Z );

			s3dSimpleShader->DisableAttribute( 0 );
			s3dSimpleShader->EndShaderProgram();

			CS3DVec3f obj, win;
			s3dProject( &obj, &win, (float*)&mvMat );
			print_text( (int)win.x, (int)win.y + 20, "World", 1 );

			obj = CS3DVec3f( 5, 0, 0 );
			s3dProject( &obj, &win, (float*)&mvMat );
			print_text( (int)win.x, (int)win.y, "X", 1 );
			
			obj = CS3DVec3f( 0, 5, 0 );
			s3dProject( &obj, &win, (float*)&mvMat );
			print_text( (int)win.x, (int)win.y, "Y", 1 );

			obj = CS3DVec3f( 0, 0, 5 );
			s3dProject( &obj, &win, (float*)&mvMat );
			print_text( (int)win.x, (int)win.y, "Z", 1 );
		}
		//*/
		
		CS3DMat16f orthoMat;
		orthoMat.ortho( 0, (float)WindowSize.cx, (float)WindowSize.cy, 0, -1, 1 );
		S3D_TexCoord tverts[4] = { {1,1},{0,1},{1,0},{0,0} };

#if ( S3D_USE_SHADOWS )
		if ( s3dSpriteShader && s3dShadow && s3dShadow->enabled )
		{
			float s = 128.f;

			CS3DMat16f m;
			m.translate( CS3DVec3f( WindowSize.cx - s - 20, 200, 0 ) );
			m = m * orthoMat;
			print_text( WindowSize.cx - (int)s - 0, 350, "Shadow map", 1 );

			S3D_Vertex verts[4] = {
				S3D_Vertex( s, 0, 0 ),
				S3D_Vertex( 0, 0, 0 ),
				S3D_Vertex( s, s, 0 ),
				S3D_Vertex( 0, s, 0 ),
			};
			s3dSpriteShader->BeginShaderProgram();
			s3dSpriteShader->EnableAttributes();
#if ( S3D_SHADOW_ALGORITHM == S3D_SOFT_SHADOW_VSM )
			S3D_OpenGLOp::EnableTexture( s3dShadow->colorTexture );
#else
			S3D_OpenGLOp::EnableTexture( s3dShadow->depthTexture );
#endif
			s3dSpriteShader->SetSampler( "textute", 0 );
			s3dSpriteShader->SetUniformMatrixf( "mvpMat", 4, (float*)&m );
			s3dSpriteShader->SetAttribPointerf( 1, 2, tverts, 0 );
			s3dSpriteShader->SetAttribPointerf( 0, 3, verts, 0 );
			S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );
			s3dSpriteShader->DisableAttributes();
			s3dSpriteShader->EndShaderProgram();
			S3D_OpenGLOp::DisableTexture();
		}
#endif
	}
}

void Control()
{
	S3D_CameraType camType = s3dCamera->GetType();
	float dx, dy, scalarRot, scalarPos;
	CS3DVec3f shift;
	bool inverseMouse = true;
	
	dx = (float)(state.pressed_pos.x - state.mouse_pos.x) / WindowSize.cx * ( inverseMouse ? -1 : 1 );
	dy = (float)(state.pressed_pos.y - state.mouse_pos.y) / WindowSize.cy * ( inverseMouse ? -1 : 1 );
	if ( state.control_state == 0 )
	{
		if ( state.left_button ) state.control_state = 1;
		else if ( state.arrow_keys[0] || state.arrow_keys[1] 
			   || state.arrow_keys[2] || state.arrow_keys[3] || state.mouse_whell != 0 ) state.control_state = 2;
	}

	CS3DVec3f direct = state.last_cam_position - s3dCamera->GetTarget();
	
	if ( state.control_state == 1 )
	{
		if ( state.left_button )
		{
			if ( dy >= 0 )
				direct.y = 0;
			float dist = direct.magnitude();
			state.pressed_pos = state.mouse_pos;
			state.last_cam_position = s3dCamera->GetPosition();
			scalarPos = camType == S3D_FREE_CAMERA ? 200.f : dist * 2;
			shift.x += dx * scalarPos;
			shift.y -= dy * scalarPos;
		}
		else
			state.control_state = 0;
	}
	else if ( state.control_state == 2 )
	{
		float dist = direct.magnitude();
		state.last_cam_position = s3dCamera->GetPosition();
		scalarPos = camType == S3D_FREE_CAMERA ? 10.f : dist * 0.03f;
		if ( state.arrow_keys[0] /*LEFT*/ )	shift.x += scalarPos;
		if ( state.arrow_keys[1] /*TOP*/ || state.mouse_whell < 0 ) shift.z += scalarPos + state.mouse_whell;
		if ( state.arrow_keys[2] /*RIGHT*/ )	shift.x -= scalarPos;
		if ( state.arrow_keys[3] /*DOWN*/ || state.mouse_whell > 0 ) shift.z -= scalarPos - state.mouse_whell;
		if ( !state.arrow_keys[0] && !state.arrow_keys[1] && !state.arrow_keys[2] && !state.arrow_keys[3] ) state.control_state = 0;
	}
	state.mouse_whell = 0;
	if ( state.control_state == 0 )
	{
		state.last_cam_position = s3dCamera->GetPosition();
	}

	CS3DModel *firstModel = g_Models[0];
	if ( firstModel )
	{
#if ( S3D_USE_PHYSICS )
		s3dControlCharacter( wsad_keys[0], wsad_keys[2], wsad_keys[1], wsad_keys[3], space_key ); 
#endif
		bool changeAnim = false;
		if ( state.wsad_keys[0] == 2 || state.wsad_keys[1] == 2 || state.wsad_keys[2] == 2 || state.wsad_keys[3] == 2 )
			changeAnim = true;
		else if ( state.wsad_keys[0] == 1 || state.wsad_keys[1] == 1 || state.wsad_keys[2] == 1 || state.wsad_keys[3] == 1 )
			changeAnim = true;
		if ( changeAnim )
		{
			unsigned curId = firstModel->AnimationId();
			if ( curId != 1 )
				state.last_animation_id = firstModel->AnimationId();
			for ( int j = 0; j < g_numModels; j++ )
			{
				if ( g_ModelGroup[0] == g_ModelGroup[j] )
				{
					g_Models[j]->SetAnimation( 1 );
				}
			}
		}
		else
		{
			for ( int j = 0; j < g_numModels; j++ )
			{
				if ( g_ModelGroup[0] == g_ModelGroup[j] )
				{
					g_Models[j]->SetAnimation( state.last_animation_id );
				}
			}
		}
	}
	for ( int i = 0; i < 4; i++ )
		if ( state.wsad_keys[i] == 1 ) state.wsad_keys[i] = 2;
	if ( state.space_key == 1 ) state.space_key = 2;

#if ( S3D_USE_PHYSICS )
	s3dSimulationPhysics();
#endif
#if ( S3D_USE_PHYSICS )
	if ( firstModel )//&& firstModel->AnimationCount() )
	{
		CS3DBox box;
		for ( int j = 0; j < g_numModels; j++ )
		{
			if ( g_ModelGroup[0] == g_ModelGroup[j] )
			{
				CS3DBox origBox;
				g_Models[j]->GetOriginalBox( origBox );
				box.Set( origBox.GetBBox() );
			}
		}
		// test physics
		if ( player.handle == 0 )
		{
			CS3DVec3f size = box.Size();
			player.mass = 0.f;
			player.mat.identity();
			player.mat.translate( box.Center() );
			float r = size.y * .5f;
			if ( size.z - size.y < 0.f )
			{
				r = size.z * .5f;
			}
			s3dSetAsPlayer( player, r, max( size.z - r * 2, 0.0f ), size.z * 0.1f, 200.f, size.z * 0.8f, 0.03f );
		}
		else
		{
			s3dUpdatePhysicalObject( player );
		}
		CS3DVec3f pos;
		for ( int j = 0; j < g_numModels; j++ )
		{
			if ( g_ModelGroup[0] == g_ModelGroup[j] )
			{
				CS3DMat16f *rotMat = g_Models[j]->GetRotation();
				memcpy( rotMat, &player.mat, sizeof( float ) * 12 );
				pos.set( &player.mat[12] );
				g_Models[j]->SetPosition( player.mat * (g_Models[j]->GetPivot() - box.Center()) );
			}
		}
	}
#endif

	if ( camType == S3D_FREE_CAMERA && state.right_button )
	{	
		scalarRot = 2.0f;
		s3dCamera->AddRotation( S3D_AXIS_Y, dx * scalarRot );
		s3dCamera->AddRotation( S3D_AXIS_X, dy * scalarRot );
	}

	CS3DMat16f invRotMat = s3dCamera->GetRotationMat();
	invRotMat.inverse();
	CS3DVec3f rotPosition = invRotMat * shift;
	CS3DVec3f camPosition = state.last_cam_position - rotPosition;

	if ( camType == S3D_TARGET_CAMERA && state.left_button )
	{
		float dist1 = ( state.last_cam_position - s3dCamera->GetTarget() ).magnitude();
		float dist2 = ( camPosition  - s3dCamera->GetTarget() ).magnitude();
		if ( dist2 > 0.f && dist1 != dist2 )
		{
			camPosition = S3D_MIX( s3dCamera->GetTarget(), camPosition, dist1 / dist2 );
		}
	}

	s3dCamera->SetPosition( camPosition );
	
	if ( firstModel && s3dCamera->GetType() == S3D_TARGET_CAMERA )
	{
		firstModel->ApplyTransforms();
		CS3DVec3f target = *firstModel->GetM() * firstModel->GetPivot();
		s3dCamera->SetTarget( target );
	}
	s3dCamera->Update();
	
	if ( state.light_pos_as_cam_pos )
	{
		s3dLight->GetLight( 0 )->pos = s3dCamera->GetPosition();
	}
	for ( int i = 0; i < g_numModels; i++ )
	{
		if ( state.tracking_camera )
		{
			S3D_Skeleton *Skeleton = g_Models[i]->GetSkeleton();
			if ( Skeleton )
			{
				S3D_Bone *Bone = Skeleton->GetBoneByName( "head", true );
				if ( Bone )
				{
					CS3DMat16f invModelMat = *g_Models[i]->GetM();
					invModelMat.inverse();
					CS3DVec3f mEye = invModelMat * s3dCamera->GetPosition();
					
					S3D_Transform t;
					g_Models[i]->GetCurrentTransform( Bone->index, t );
					CS3DMat16f transfMat;
					S3D_TransformOp::to_matrix( transfMat, t );

					CS3DVec3f mDirect = mEye - transfMat * Bone->joint;
					if ( mDirect.y < 0 ) // clip by Y
					{
						float rad = atan2( -mDirect.y, mDirect.z );
						float angle = rad / S3D_PI * 180;
						if ( angle > 20 && angle < 160 ) // clip by Z (20deg, 160deg)
						{
							CS3DMat16f defDirectMat;
							const float defDirectMatData[16] = {
								-1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1
							};
							defDirectMat.set( defDirectMatData );

							CS3DMat16f invDirectMat;
							invDirectMat.lookAt( CS3DVec3f(), mDirect, CS3DVec3f(0, 0, 1) );
							invDirectMat.simpleInverse();

							CS3DMat16f mat = defDirectMat * invDirectMat;

							g_Models[i]->ClearBoneTransforms();
							g_Models[i]->AddTransformToBoneByID( Bone->index, mat );
						}
					}
				}
			}
		}
	}
}

void RenderScene()
{
	Control();
	PushMatrices();
	glEnable( GL_DEPTH_TEST );

#if ( S3D_USE_SHADOWS )
	if ( s3dShadow )
	{
		std::vector< CS3DTransformedObject* > objects;
		objects.push_back( &groundBox );
		for ( int i = 0; i < g_numModels; i++ )
		{
			objects.push_back( g_Models[i] );
		}
#if ( S3D_USE_PHYSICS )
#ifdef TEST_BOXES
		for ( int i = 0; i < numBoxes; i++ )
		{
			objects.push_back( &testBoxes[i] );
		}
#endif
#endif
		s3dShadow->Draw( &objects[0], objects.size() );
	}
#endif

	glMatrixMode( GL_PROJECTION );
	s3dPerspective( S3D_FOVY, S3D_NEAR, S3D_FAR );
	glLoadMatrixf( (float*)&s3dProjection );
	glMatrixMode( GL_MODELVIEW );

	DrawGround();
	DrawLightSource();

	state.video_mem_alloted = (float)s3dGetTexMemAlloted();
	if ( s3dBuffer )
		state.video_mem_alloted += (float)s3dBuffer->MemAlloted();

	selectedModel.name = "{null}";
	selectedModel.id = -1;
	
	for ( int i = 0; i < g_numModels; i++ )
	{
		g_Models[i]->Draw();
		if ( state.left_button )
		{
			if ( g_Models[i]->Hover( state.mouse_pos.x, state.mouse_pos.y ) )
			{
				selectedModel.name = g_Models[i]->GetName();
				selectedModel.id = i;
			}
		}
	}

	if ( debugInfo.enabled )
	{
#if ( S3D_USE_PHYSICS )
		if ( g_Models[0] && debugInfo.collision_shape )
		{
			glEnable( GL_CULL_FACE );
			glCullFace( GL_FRONT );
			const CS3DMat16f *mvMat = g_Models[0]->GetMV();
			CS3DMat16f mat = *mvMat;
			CS3DBox box;
			for ( int j = 0; j < g_numModels; j++ )
			{
				if ( g_ModelGroup[0] == g_ModelGroup[j] )
				{
					box.Set( g_Models[j]->GetBBList()->parent.GetBBox().max );
					box.Set( g_Models[j]->GetBBList()->parent.GetBBox().min );
				}
			}
			mat.translate( box.Center() );
			glPushMatrix();
			glLoadMatrixf( (float*)&mat );
			glColor4f( 1, 1, 1, 0.4f );
			CS3DVec3f size = box.Size();
			float r = size.y * .5f;
			if ( size.z - size.y < 0.f )
			{
				r = size.z * .5f;
			}
			Shapes::DrawCapsule( r, max( size.z - r * 2, 0.0f ), 10, 10 );
			glPopMatrix();
			glDisable( GL_CULL_FACE );
		}
#endif
		if ( debugInfo.bboxes )
		{
			for ( int i = 0; i < g_numModels; i++ )
				g_Models[i]->DrawBBoxes();
		}
	}
	glDisable( GL_DEPTH_TEST );
	if ( debugInfo.enabled )
	{
		if ( debugInfo.skeletion )
			for ( int i = 0; i < g_numModels; i++ )
				g_Models[i]->DrawSkeleton();
		if ( debugInfo.pivot )
		{
			for ( int i = 0; i < g_numModels; i++ )
				g_Models[i]->DrawPivot();
		}
		for ( int i = 0; i < g_numModels; i++ )
			g_Models[i]->DrawSceneWalls();
	}

	/*
#if ( S3D_USE_SHADOWS )
	if ( s3dSpriteShader && s3dShadow && s3dShadow->enabled )
	{
		S3D_TexCoord tverts[4] = { {1,1},{0,1},{1,0},{0,0} };
		float s = 1024;
		S3D_Vertex verts[4] = {
			S3D_Vertex( s, s, 0 ),
			S3D_Vertex( 0, s, 0 ),
			S3D_Vertex( s, 0, 0 ),
			S3D_Vertex( 0, 0, 0 )
		};
		s3dOrtho( -1000, 1000 );
		s3dSpriteShader->BeginShaderProgram();
		s3dSpriteShader->EnableAttributes();
		S3D_OpenGLOp::EnableTexture( s3dShadow->colorTexture, 0 );
		s3dSpriteShader->SetSampler( "textute", 0 );
		s3dSpriteShader->SetUniformMatrixf( "mvpMat", 4, (float*)&s3dProjection );
		s3dSpriteShader->SetAttribPointerf( 1, 2, tverts, 0 );
		s3dSpriteShader->SetAttribPointerf( 0, 3, verts, 0 );
		S3D_OpenGLOp::DrawPrimitives( S3D_TRIANGLE_STRIP, 0, 4 );
		s3dSpriteShader->DisableAttributes();
		s3dSpriteShader->EndShaderProgram();
		s3dPerspective( S3D_FOVY, S3D_NEAR, S3D_FAR );
	}
#endif
	//*/

	PopMatrices();
	PrintInfo();
}