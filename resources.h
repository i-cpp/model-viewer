#define IDD_CHANGE_LIGHT_POS 10
#define IDC_CHANGE_LIGHT_ID 11
#define IDC_CHANGE_LIGHT_POSX 12
#define IDC_CHANGE_LIGHT_POSX_SPINNER 13
#define IDC_CHANGE_LIGHT_POSY 14
#define IDC_CHANGE_LIGHT_POSY_SPINNER 15
#define IDC_CHANGE_LIGHT_POSZ 16
#define IDC_CHANGE_LIGHT_POSZ_SPINNER 17
#define IDC_CHANGE_LIGHT_APPLY 18
#define IDC_CHANGE_LIGHT_CANCEL 19
#define IDC_STATIC -1

#define IDI_ICON 100
#define IDR_MYMENU 101


#define ID_FILE_EXIT 1001

#define ID_MODEL_OPEN 2001
#define ID_MODEL_SHADOW 2002
#define ID_MODEL_TRACKING_CAMERA 2003
#define ID_MODEL_POLY_MODE_FILL 20001
#define ID_MODEL_POLY_MODE_LINE 20002
#define ID_MODEL_CLOSE 2007
#define ID_MODEL_CLOSE_ALL 2008

#define ID_DEBUG_ENABLE 3001
#define ID_DEBUG_MODEL_BBOXES 30001
#define ID_DEBUG_MODEL_PIVOT 30002
#define ID_DEBUG_MODEL_SKELETON 30003
#define ID_DEBUG_MODEL_COLLISION_SHAPE 30004

#define ID_LIGHTING_LOAD 4001
#define ID_LIGHTING_CHANGE_POS 4002
#define ID_LIGHTING_AS_CAM_POS 4003

#define ID_CAMERA_TYPE_FREE 50001
#define ID_CAMERA_TYPE_TARGET 50002

#define ID_HELP_CONTROL 6001
#define ID_HELP_ABOUT 6002
