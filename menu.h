#ifndef _menu_h_
#define _menu_h_

void CreateAppMenu(const State &state, const DebugInfo &debugInfo);
LRESULT CALLBACK EditProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK DlgProc(HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam);

#endif //_menu_h_