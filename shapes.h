#ifndef shapes_h
#define shapes_h

namespace Shapes {
	void DrawSphere( const float center[], float radius, int slices, int stacks, int vertHalf = 2 );
	void DrawCylinder( float baseRadius, float topRadius, float height, int slices, int stacks );
	void DrawCapsule( float radius, float height, int slices, int stacks );
	void DrawGrid( float width, float height, float z, int slices, int stacks );
};

#endif
