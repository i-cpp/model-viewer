#ifndef _common_h_
#define _common_h_

#include <S3D.h>
#include <windows.h>
#include <GL/gl.h>

#include <crtdbg.h>
#define new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#define malloc(s) _malloc_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)
#define calloc(c, s) _calloc_dbg(c, s, _NORMAL_BLOCK, __FILE__, __LINE__)
#define realloc(p, s) _realloc_dbg(p, s, _NORMAL_BLOCK, __FILE__, __LINE__)
#define _strdup(s) _strdup_dbg(s, _NORMAL_BLOCK, __FILE__, __LINE__)

namespace texture {
	unsigned int load( const char *filename, S3D_Image *img, bool gen_texture );
	unsigned int load_from_data( unsigned char *data, int w, int h, int bpp, bool linear_filter, bool gen_mip_maps );
	unsigned int total_size();
	void free_all();
};

void print_text( int x, int y, const char *text, int interval );
unsigned int real_time();

extern HINSTANCE		hInstance;
extern HWND				hMainWnd;
extern unsigned int		CurTime;
extern SIZE				WindowSize;

#include "defs.h"

#endif // _common_h_